/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"
#include "../narupacppclient/NarupaClient.h"

typedef AudioProcessorValueTreeState::SliderAttachment SliderAttachment;
//==============================================================================
/**
*/
class AudioClientAudioProcessorEditor  : public  AudioProcessorEditor,
                                         private Timer
{
public:
    AudioClientAudioProcessorEditor (AudioClientAudioProcessor&);
    ~AudioClientAudioProcessorEditor();

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;

private:
    void timerCallback() override;
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    AudioClientAudioProcessor& processor;

    Label kineteicEnergyLabel;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (AudioClientAudioProcessorEditor)
};
