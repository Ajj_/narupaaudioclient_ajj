/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
AudioClientAudioProcessorEditor::AudioClientAudioProcessorEditor (AudioClientAudioProcessor& p)
    : AudioProcessorEditor (&p), 
      processor (p)
{
    kineteicEnergyLabel.setText ("", dontSendNotification);
    addAndMakeVisible(kineteicEnergyLabel);

    setSize (400, 300);

    startTimerHz (20);
}

AudioClientAudioProcessorEditor::~AudioClientAudioProcessorEditor()
{
}

//==============================================================================
void AudioClientAudioProcessorEditor::paint (Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));
}

void AudioClientAudioProcessorEditor::resized()
{
    Rectangle<int> r = getLocalBounds();
    kineteicEnergyLabel.setBounds (r.removeFromTop (20));
}

void AudioClientAudioProcessorEditor::timerCallback()
{
    kineteicEnergyLabel.getTextValue() = "Kinetic Energy: " + String (processor.getKineticEnergy());
}