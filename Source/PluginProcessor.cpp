/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"
#include <algorithm>

//==============================================================================
AudioClientAudioProcessor::AudioClientAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
    : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", AudioChannelSet::stereo(), true)
                     #endif
                       ),
      window (50)
#endif
{
    narupaClient.setPositionCallback ([](std::vector<float> positions)
    {
        for (auto p : positions)
        {
            //DBG (p);
        }
    });

    narupaClient.setKinEnergyCallback([&](float value)
    {
        //DBG("kin en: " << kinEnergy_);
        const auto windowVal = window.getNormalisedValue (value);
        const auto clamp = std::clamp (windowVal, 0.f, 1.f);
        
        kineticEnergy = std::powf (clamp, 3);
    });

    narupaClient.setChainCountCallback([](int chainCount)
    {
         DBG("chain count: " << chainCount);
    });
}

AudioClientAudioProcessor::~AudioClientAudioProcessor()
{
}

//==============================================================================
const String AudioClientAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool AudioClientAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool AudioClientAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool AudioClientAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double AudioClientAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int AudioClientAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int AudioClientAudioProcessor::getCurrentProgram()
{
    return 0;
}

void AudioClientAudioProcessor::setCurrentProgram (int index)
{
}

const String AudioClientAudioProcessor::getProgramName (int index)
{
    return {};
}

void AudioClientAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void AudioClientAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{

}

void AudioClientAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool AudioClientAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    if (layouts.getMainOutputChannelSet() != AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void AudioClientAudioProcessor::processBlock (AudioBuffer<float>& buffer, MidiBuffer& midiMessages)
{  

    ScopedNoDenormals noDenormals;
    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();

    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());

    // get the kinetic energy 
    auto ke = kineticEnergy.load();
    auto keCC = ke * 127.0f;

    auto message = MidiMessage::controllerEvent(1, 10, keCC);

    addMessageToBuffer(message, midiMessages, getSampleRate());

    for (int channel = 0; channel < totalNumInputChannels; ++channel)
    {
        auto* channelData = buffer.getWritePointer (channel);    
    }
}

void AudioClientAudioProcessor::addMessageToBuffer(const MidiMessage& message, MidiBuffer& buffer, double sampleRate)
{
    auto timestamp = message.getTimeStamp();
    auto sampleNumber = (int)(timestamp * sampleRate);
    buffer.addEvent(message, sampleNumber);
}

//==============================================================================
bool AudioClientAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* AudioClientAudioProcessor::createEditor()
{
    return new AudioClientAudioProcessorEditor (*this);
}

//==============================================================================
void AudioClientAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void AudioClientAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new AudioClientAudioProcessor();
}
